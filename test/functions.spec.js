const functions = require('../src/functions');

const flipSwitch = functions.flipSwitch;

describe('flipSwitch', () => {
    it(`Should work correctly.`, () => {
        expect(flipSwitch(["aswq", "flip", "as", "bit"])).to.eql([true, false, false, false]);
        expect(flipSwitch(["flip", 11034, 2, 1])).to.eql([false, false, false, false]);
        expect(flipSwitch([false, false, "flip", "sheep", "flip"])).to.eql([true, true, false, false, true]);
        expect(flipSwitch(["aswq", "Flip", "as", "bit"])).to.eql([true, true, true, true]);
    });
    it(`The result should return an array.`, () => {
        expect(flipSwitch(["aswq", "flip", "as", "bit"])).to.be.an('array');
        expect(primeNumbers(["flip", 11034, 2, 1])).to.be.an('array');
    });
});
