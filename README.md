# Loops

## Task #1

## Flip switch
### Create a function called "flipSwitch" that always returns true for every item in a given array. However, if an element is the word "flip", switch to always returning the opposite boolean value.

### For example: 
```
flipSwitch(["aswq", "flip", "as", "bit"]) -> [true, false, false, false]
flipSwitch(["flip", 11034, 2, 1]) -> [false, false, false, false]
flipSwitch([false, false, "flip", "sheep", "flip"]) -> [true, true, false, false, true]
```
### Notes
- "flip" will always be given in lowercase.
- an array may contain multiple flips.
- switch the boolean value on the same element as the flip itself.
